package flightApplication.flight;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FlightsMapper implements ResultSetMapper<Flight> {

    @Override
    public Flight map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Flight(
                resultSet.getLong("Id"),
                resultSet.getString("where_from"),
                resultSet.getString("where_to"),
                resultSet.getDate("flight_date"),
                resultSet.getDate("arrival_date"),
                resultSet.getInt("seat_count"),
                resultSet.getInt("tourist_count"),
                resultSet.getBigDecimal("ticket_price")
        );
    }
}
