package flightApplication.flight;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/flights")
@Produces(MediaType.APPLICATION_JSON)
public class FlightResource {
    private FlightService flightService;

    public FlightResource(FlightService flightService) {
        this.flightService = flightService;
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    public Long saveFlight(@Valid SaveFlightRequest saveFlightRequest) {
        return flightService.saveFlight(saveFlightRequest);
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    @Path("/addTouristFlight")
    public void addTouristFlight(@Valid AddTouristFlightRequest addTouristFlightRequest) {
        flightService.addTouristFlight(addTouristFlightRequest);
    }

    @Path("/{id}")
    @DELETE
    public void Flight(@PathParam("id") Long id) {
        flightService.removeFlight(id);
    }

    @Path("/flightsForTourist")
    @GET
    public List<Flight> findFlightsForTourist(@QueryParam("touristId") int touristId) {
        return flightService.findFlightsForTourist(new Long(touristId));
    }

    @Path("/flightsForTourist")
    @DELETE
    public void removeTouristsFlight(@QueryParam("flightId") Long flightId, @QueryParam("touristId") Long touristId) {
        flightService.removeTouristFlight(flightId, touristId);
    }

    @GET
    public List<Flight> findFlights() {
        return flightService.findFlights();
    }

    @Path("/{id}")
    @GET
    public Flight getFlight(@PathParam("id") Long id) {
        return flightService.getFlight(id);
    }

    @Path("/tourists_flights_ids/{id}")
    @GET
    public List<Long> findTouristsIdsByFlightId(@PathParam("id") Long id) {
        return flightService.findTouristsIdsByFlightId(id);
    }
}
