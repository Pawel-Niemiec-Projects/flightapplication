package flightApplication.flight;

import flightApplication.tourist.Tourist;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Flight {
    @NotNull
    private Long id;
    @NotNull
    private String whereFrom;
    @NotNull
    private String whereTo;
    @NotNull
    private Date flightDate;
    @NotNull
    private Date arrivalDate;
    @NotNull
    @Min(1)
    private int seatCount;
    private int touristCount;
    @NotNull
    private BigDecimal ticketPrice;
    private List<Tourist> tourists;

    public Flight() {
        //for automatic deserialization
    }

    public Flight(Long id, String whereFrom, String whereTo, Date flightDate, Date arrivalDate, int seatCount, int touristCount, BigDecimal ticketPrice) {
        this.id = id;
        this.whereFrom = whereFrom;
        this.whereTo = whereTo;
        this.flightDate = flightDate;
        this.arrivalDate = arrivalDate;
        this.seatCount = seatCount;
        this.touristCount = touristCount;
        this.ticketPrice = ticketPrice;
    }

    public Flight(Flight flight, List<Tourist> tourists) {
        this.id = id;
        this.whereFrom = flight.whereFrom;
        this.whereTo = flight.whereTo;
        this.flightDate = flight.flightDate;
        this.arrivalDate = flight.arrivalDate;
        this.seatCount = flight.seatCount;
        this.touristCount = flight.touristCount;
        this.ticketPrice = flight.ticketPrice;
        this.tourists = tourists;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWhereFrom() {
        return whereFrom;
    }

    public void setWhereFrom(String whereFrom) {
        this.whereFrom = whereFrom;
    }

    public String getWhereTo() {
        return whereTo;
    }

    public void setWhereTo(String whereTo) {
        this.whereTo = whereTo;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public int getTouristCount() {
        return touristCount;
    }

    public void setTouristCount(int touristCount) {
        this.touristCount = touristCount;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public List<Tourist> getTourists() {
        return tourists;
    }

    public void setTourists(List<Tourist> tourists) {
        this.tourists = tourists;
    }
}