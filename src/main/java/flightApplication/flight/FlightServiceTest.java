package flightApplication.flight;

import flightApplication.tourist.Tourist;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.skife.jdbi.v2.DBI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class FlightServiceTest {


    @Mock
    private FlightDao flightDaoMock;

    @Mock
    private DBI jdbiMock;

    @Mock
    private List<Tourist> tourists = new ArrayList<>();

    @InjectMocks
    private FlightService flightService;

    @Test
    public void saveFlightTest() {
        when(flightDaoMock.saveFlight(jdbiMock.open(), any(SaveFlightRequest.class)))
                .thenReturn(new Long(1));
        SaveFlightRequest saveFlightRequest = new SaveFlightRequest();
        assertThat(flightService.saveFlight(saveFlightRequest)).isInstanceOf(Long.class);
    }

    @Test
    public void findTouristsIdsByFlightId() {
        when(flightDaoMock.findFTouristsForFlight(jdbiMock.open(), new Long(0))).thenReturn(new ArrayList<>());
        List<Long> touristsIds = flightDaoMock.findFTouristsForFlight(jdbiMock.open(), new Long(0))
                .stream()
                .map(tourist -> {
                    return tourist.getId();
                })
                .collect(Collectors.toList());;
        assertThat(touristsIds).contains(7L);
    }

/*    @Test
    public void getFlightTest(Long id) {
        when(flightDaoMock.getFlight(jdbi.open(), id)).thenReturn(new Flight());
        assertThat(flightService.getFlight(id), );
    }*/
}
