package flightApplication.flight;

public class AddTouristFlightRequest {
    private Long touristId;
    private Long flightId;

    public AddTouristFlightRequest() {
    }

    public AddTouristFlightRequest(Long touristId, Long flightId) {
        this.touristId = touristId;
        this.flightId = flightId;
    }

    public Long getTouristId() {
        return touristId;
    }

    public void setTouristId(Long touristId) {
        this.touristId = touristId;
    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }
}
