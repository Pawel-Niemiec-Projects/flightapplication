package flightApplication.flight;

import org.skife.jdbi.v2.DBI;

import java.util.List;
import java.util.stream.Collectors;

public class FlightService {
    private FlightDao flightDao;
    private DBI jdbi;

    public FlightService(FlightDao flightDao, DBI jdbi) {
        this.flightDao = flightDao;
        this.jdbi = jdbi;
    }

    public Long saveFlight(SaveFlightRequest saveFlightRequest) {
        return jdbi.inTransaction(((handle, transactionStatus) -> {
            return flightDao.saveFlight(handle, saveFlightRequest);
        }));
    }

    public void addTouristFlight(AddTouristFlightRequest addTouristFlightRequest) {
        jdbi.useTransaction(((handle, transactionStatus) -> {
            flightDao.addTouristFlight(handle, addTouristFlightRequest);
        }));
    }

    public void removeFlight(Long id) {
        jdbi.useTransaction(((handle, transactionStatus) -> {
            flightDao.removeFlight(handle, id);
        }));
    }

    public void removeTouristFlight(Long flightId, Long touristId) {
        jdbi.useTransaction(((handle, transactionStatus) -> {
            flightDao.removeTouristFlight(handle, flightId, touristId);
        }));
    }

    public List<Flight> findFlights() {
        return jdbi.withHandle((handle -> {
            return flightDao.findFlights(handle);
        }));
    }

    public List<Flight> findFlightsForTourist(Long id) {
        return jdbi.withHandle((handle -> {
            return flightDao.findFlightsForTourist(handle, id);
        }));
    }

    public Flight getFlight(Long id) {
        return jdbi.withHandle(handle -> {
            return flightDao.getFlight(handle, id);
        });
    }

    public List<Long> findTouristsIdsByFlightId(Long id) {

        List<Long> touristsIds=jdbi.withHandle(handle -> {
            return flightDao.findFTouristsForFlight(handle, id);
        })
                .stream()
                .map((tourist -> {
                    return tourist.getId();
                }))
                .collect(Collectors.toList());
        System.out.println(touristsIds);
        return touristsIds;
    }
}
