package flightApplication.flight;

import flightApplication.tourist.Tourist;
import flightApplication.tourist.TouristMapper;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.util.LongColumnMapper;

import java.util.List;

public class FlightDao {
    private FlightsMapper flightsMapper;

    public FlightDao() {
        this.flightsMapper = new FlightsMapper();
    }

    public Long saveFlight(Handle handle, SaveFlightRequest saveFlightRequest) {
        String sql =
                "INSERT INTO flights " +
                        "(where_from, where_to, flight_date, arrival_date, seat_count, tourist_count, ticket_price) VALUES " +
                        "(:where_from, :where_to, :flight_date, :arrival_date, :seat_count, :tourist_count, :ticket_price)";
        return handle.createStatement(sql)
                .bind("where_from", saveFlightRequest.getWhereFrom())
                .bind("where_to", saveFlightRequest.getWhereTo())
                .bind("flight_date", saveFlightRequest.getFlightDate())
                .bind("arrival_date", saveFlightRequest.getArrivalDate())
                .bind("seat_count", saveFlightRequest.getSeatCount())
                .bind("tourist_count", saveFlightRequest.getTouristCount())
                .bind("ticket_price", saveFlightRequest.getTicketPrice())
                .executeAndReturnGeneratedKeys(LongColumnMapper.PRIMITIVE).first();
    }

    public void addTouristFlight(Handle handle, AddTouristFlightRequest addTouristFlightRequest) {
        Flight flight = getFlight(handle, addTouristFlightRequest.getFlightId());
        if (flight.getSeatCount() - flight.getTouristCount() > 0) {
            adjustFlightTouristCount(handle, addTouristFlightRequest.getFlightId(), -1);
            String sql =
                    "INSERT INTO tourists_flights " +
                            "(flight_id, tourist_id) VALUES " +
                            "(:flight_id, :tourist_id)";
            handle.createStatement(sql)
                    .bind("tourist_id", addTouristFlightRequest.getTouristId())
                    .bind("flight_id", addTouristFlightRequest.getFlightId())
                    .execute();
        }
    }


    public void removeFlight(Handle handle, Long id) {
        String sql = "DELETE FROM flights WHERE id= " + id;
        handle.execute(sql);
    }

    private void adjustFlightTouristCount(Handle handle, Long flightId, int adjustNumber) {
        String sql = "UPDATE flights SET " +
                "tourist_count= " + getFlight(handle, flightId).getTouristCount() + adjustNumber;
        handle.createStatement(sql).execute();
    }

    public void removeTouristFlight(Handle handle, Long flightId, Long touristID) {
        String sql = "DELETE * FROM tourists_flights WHERE tourists_flights.flight_id= " + flightId + " AND tourists_flights.tourist_id = " + touristID;
    }

    public List<Tourist> findFTouristsForFlight(Handle handle, Long id) {
        String sql = "SELECT * FROM tourists_flights RIGHT JOIN tourists ON tourists_flights.tourist_id=tourists.id WHERE tourists_flights.flight_id= " + id;
        return handle.createQuery(sql).map(new TouristMapper()).list();
    }

    public List<Flight> findFlightsForTourist(Handle handle, long touristId) {
        String sql = "SELECT * FROM flights RIGHT JOIN tourists_flights ON flights.id=tourists_flights.flight_id WHERE tourists_flights.tourist_id= " + touristId;
        return handle.createQuery(sql).map(flightsMapper).list();
    }

    public List<Flight> findFlights(Handle handle) {
        return handle.createQuery("SELECT * FROM flights").map(flightsMapper).list();
    }

    public Flight getFlight(Handle handle, long id) {
        String sql = "SELECT * FROM flights WHERE id= " + id;
        Flight flight = handle.createQuery(sql).map(flightsMapper).first();
        flight.setTourists(findFTouristsForFlight(handle, id));
        return flight;
    }

    public void createTouristsFlightsTable(Handle handle) {
        handle.execute("CREATE TABLE tourists_flights (" +
                "id BIGSERIAL PRIMARY KEY NOT NULL, " +
                "flight_id BIGINT NOT NULL REFERENCES flights, " +
                "tourist_id BIGINT NOT NULL REFERENCES tourists )"
        );
    }

    public void createTouristTable(Handle handle) {
        handle.execute("CREATE TABLE tourists (" +
                "id BIGSERIAL PRIMARY KEY NOT NULL, " +
                "name VARCHAR (50), " +
                "surname VARCHAR (100), " +
                "sex VARCHAR (10), " +
                "country VARCHAR (10), " +
                "notes VARCHAR (200), " +
                "birth_date DATE )");
    }

    public void createFlightTable(Handle handle) {
        handle.execute("CREATE TABLE flights (" +
                "id BIGSERIAL PRIMARY KEY NOT NULL, " +
                "where_from VARCHAR (50), " +
                "where_to VARCHAR (50), " +
                "flight_date DATE, " +
                "arrival_date DATE, " +
                "seat_count INT, " +
                "tourist_count INT, " +
                "ticket_price DECIMAL(5,2))");
    }
}
