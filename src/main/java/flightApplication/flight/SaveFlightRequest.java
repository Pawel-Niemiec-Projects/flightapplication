package flightApplication.flight;

import java.math.BigDecimal;
import java.util.Date;

public class SaveFlightRequest {
    private String whereFrom;
    private String whereTo;
    private Date flightDate;
    private Date arrivalDate;
    private int seatCount;
    private int touristCount;
    private BigDecimal ticketPrice;

    public SaveFlightRequest() {
    }

    public SaveFlightRequest(String whereFrom, String whereTo, Date flightDate, Date arrivalDate, int seatCount, int touristCount, BigDecimal ticketPrice) {
        this.whereFrom = whereFrom;
        this.whereTo = whereTo;
        this.flightDate = flightDate;
        this.arrivalDate = arrivalDate;
        this.seatCount = seatCount;
        this.touristCount = touristCount;
        this.ticketPrice = ticketPrice;
    }

    public String getWhereFrom() {
        return whereFrom;
    }

    public void setWhereFrom(String whereFrom) {
        this.whereFrom = whereFrom;
    }

    public String getWhereTo() {
        return whereTo;
    }

    public void setWhereTo(String whereTo) {
        this.whereTo = whereTo;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public int getTouristCount() {
        return touristCount;
    }

    public void setTouristCount(int touristCount) {
        this.touristCount = touristCount;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }
}
