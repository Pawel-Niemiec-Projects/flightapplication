package flightApplication.tourist;

public class DeleteTouristFlightRequest {
    private Long id;

    public DeleteTouristFlightRequest() {
    }

    public DeleteTouristFlightRequest(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
