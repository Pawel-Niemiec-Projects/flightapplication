package flightApplication.tourist;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TouristMapper implements ResultSetMapper<Tourist> {

    @Override
    public Tourist map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Tourist(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getString("surname"),
                resultSet.getString("sex"),
                resultSet.getString("country"),
                resultSet.getString("notes"),
                resultSet.getDate("birth_date"));
    }
}