package flightApplication.tourist;

import flightApplication.flight.Flight;

import java.util.Date;
import java.util.List;

public class Tourist {
    private Long id;
    private String name;
    private String surname;
    private String sex;
    private String country;
    private String notes;
    private Date birthDate;
    private List<Flight> flights;

    public Tourist() {
        //for automatic deserialization
    }

    public Tourist(Long id, String name, String surname, String sex, String country, String notes, Date birthDate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.country = country;
        this.notes = notes;
        this.birthDate = birthDate;
    }

    public Tourist(Tourist tourist, List<Flight> flights) {
        this.id = tourist.getId();
        this.name = tourist.getName();
        this.surname = tourist.getSurname();
        this.sex = tourist.getSex();
        this.country = tourist.getCountry();
        this.notes = tourist.getNotes();
        this.birthDate = tourist.birthDate;
        this.flights = flights;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }
}