package flightApplication.tourist;


import io.dropwizard.validation.*;
import org.hibernate.validator.constraints.NotEmpty;


import java.util.Date;

public class SaveTouristRequest {
    @NotEmpty
    private String name;
    @NotEmpty
    private String surname;
    @NotEmpty
    @OneOf(value = {"male", "female"}, ignoreCase = true, ignoreWhitespace = true)
    private String sex;
    @NotEmpty
    private String country;
    private String notes;
    @NotEmpty
    private Date birthDate;

    public SaveTouristRequest() {
    }

    public SaveTouristRequest(String name, String surname, String sex, String country, String notes, Date birthDate) {
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.country = country;
        this.notes = notes;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

}
