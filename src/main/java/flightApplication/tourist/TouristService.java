package flightApplication.tourist;

import flightApplication.flight.Flight;
import org.skife.jdbi.v2.DBI;

import java.util.List;

public class TouristService {
    private TouristDao touristDao;
    private DBI jdbi;

    public TouristService(TouristDao touristDao, DBI jdbi) {
        this.touristDao = touristDao;
        this.jdbi = jdbi;
    }

    public Long saveTourist(SaveTouristRequest saveTouristRequest) {
        return jdbi.inTransaction(((handle, transactionStatus) -> {
            return touristDao.saveTourist(handle, saveTouristRequest);
        }));
    }

    public void removeTourist(Long id) {
        jdbi.useTransaction(((handle, transactionStatus) -> {
            touristDao.removeTourist(handle, id);
        }));
    }
    public void removeTouristFlight(DeleteTouristFlightRequest deleteTouristFlightRequest){
        jdbi.useTransaction(((handle, transactionStatus) -> {touristDao.removeTouristFlight(handle, deleteTouristFlightRequest);}));
    }

    public List<Tourist> findTouristsForFlight(Long id) {
        return jdbi.withHandle((handle) -> {
            return touristDao.findTouristsForFlight(handle, id);
        });
    }

    public List<Flight> findTouristFlights(Long id) {
        return jdbi.withHandle(handle -> {
            return touristDao.findTouristFlights(handle, id);
        });
    }

    public Tourist getTourist(Long id) {
        return jdbi.withHandle(handle -> {
            return touristDao.getTourist(handle, id);
        });
    }

    public List<Tourist> findTourists() {
        return jdbi.withHandle((handle -> {
            return touristDao.findTourists(handle);
        }));
    }
}