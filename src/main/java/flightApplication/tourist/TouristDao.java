package flightApplication.tourist;

import flightApplication.flight.Flight;
import flightApplication.flight.FlightsMapper;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.util.LongColumnMapper;

import java.util.List;
import java.util.stream.Collectors;

public class TouristDao {
    private TouristMapper touristMapper;

    public TouristDao() {
        this.touristMapper = new TouristMapper();
    }

    public Long saveTourist(Handle handle, SaveTouristRequest saveTouristRequest) {
        String sql =
                "INSERT INTO tourists" +
                        "(name, surname, sex, country, notes, birth_date) VALUES " +
                        "(:name, :surname, :sex, :country, :notes, :birth_date)";
        return handle.createStatement(sql)
                .bind("name", saveTouristRequest.getName())
                .bind("surname", saveTouristRequest.getSurname())
                .bind("sex", saveTouristRequest.getSex())
                .bind("country", saveTouristRequest.getCountry())
                .bind("notes", saveTouristRequest.getNotes())
                .bind("birth_date", saveTouristRequest.getBirthDate())
                .executeAndReturnGeneratedKeys(LongColumnMapper.PRIMITIVE).first();
    }

    public void removeTourist(Handle handle, Long id) {
        String sql = "DELETE FROM tourists WHERE id= " + id;
        handle.execute(sql);
    }

    public void removeTouristFlight(Handle handle, DeleteTouristFlightRequest deleteTouristFlightRequest) {
        String sql = "DELETE FROM tourists_flights WHERE id= " + deleteTouristFlightRequest.getId();
        handle.execute(sql);
    }


    public List<Flight> findTouristFlights(Handle handle, Long id) {
        String sql =
                "SELECT * FROM tourists_flights RIGHT JOIN flights ON tourists_flights.flight_id=flights.id WHERE tourists_flights.tourist_id= " + id;
        return handle.createQuery(sql).map(new FlightsMapper()).list();
    }

    public List<Tourist> findTouristsForFlight(Handle handle, Long flightId) {
        String sql =
                "SELECT * FROM tourists LEFT JOIN tourists_flights ON tourists.id=tourists_flights.tourist_id WHERE tourists_flights.flight_id= " + flightId;
        return handle.createQuery(sql).map(new TouristMapper()).list();
    }

    public List<Tourist> findTourists(Handle handle) {
        List<Tourist> tourists = handle.createQuery("SELECT * FROM tourists").map(touristMapper).list();
        return tourists.stream().map((tourist) -> {
            tourist.setFlights(findTouristFlights(handle, tourist.getId()));
            return tourist;
        }).collect(Collectors.toList());
    }

    public Tourist getTourist(Handle handle, Long touristId) {
        String sql = "SELECT * FROM tourists WHERE id= " + touristId;
        Tourist tourist = handle.createQuery(sql).map(touristMapper).first();
        tourist.setFlights(findTouristFlights(handle, touristId));
        return tourist;
    }
}