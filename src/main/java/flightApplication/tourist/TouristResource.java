package flightApplication.tourist;

import flightApplication.flight.Flight;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/tourists")
@Produces(MediaType.APPLICATION_JSON)
public class TouristResource {
    private TouristService touristService;

    public TouristResource(TouristService touristService) {
        this.touristService = touristService;
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    public Long saveTourist(SaveTouristRequest saveTouristRequest) {
        return touristService.saveTourist(saveTouristRequest);
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @DELETE
    public void removeTourist(@PathParam("id") Long id) {
        touristService.removeTourist(id);
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/deleteFlight")
    @DELETE
    public void removeTouristFlight(DeleteTouristFlightRequest deleteTouristFlightRequest) {
        touristService.removeTouristFlight(deleteTouristFlightRequest);
    }

    @Path("/touristsForFlight")
    @GET
    public List<Tourist> findTouristsForFlight(@Valid @QueryParam("flightId") int flightId) {
        return touristService.findTouristsForFlight(new Long(flightId));
    }

    @GET
    public List<Tourist> findTourists() {
        return touristService.findTourists();
    }

    @Path("/{id}/flights")
    @GET
    public List<Flight> findTouristFlights(@PathParam("id") Long id) {
        return touristService.findTouristFlights(id);
    }

    @Path("/{id}")
    @GET
    public Tourist getTourist(@PathParam("id") Long id) {
        return touristService.getTourist(id);
    }
}
