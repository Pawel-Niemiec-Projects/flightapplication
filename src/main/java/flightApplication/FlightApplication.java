package flightApplication;

import flightApplication.flight.FlightDao;
import flightApplication.flight.FlightResource;
import flightApplication.flight.FlightService;
import flightApplication.tourist.TouristDao;
import flightApplication.tourist.TouristResource;
import flightApplication.tourist.TouristService;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;


public class FlightApplication extends Application<FlightApplicationConfiguration> {
    public static void main(String[] args) throws Exception {
        new FlightApplication().run(args);
    }

    public void run(FlightApplicationConfiguration configuration, Environment environment) throws Exception {
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "postgresql");
        final FlightDao flightDao = jdbi.onDemand(FlightDao.class);
        final TouristDao touristDao = jdbi.onDemand(TouristDao.class);
        environment.jersey().register(new FlightResource(new FlightService(flightDao, jdbi)));
        environment.jersey().register(new TouristResource(new TouristService(touristDao, jdbi)));
    }


    @Override
    public void initialize(Bootstrap<FlightApplicationConfiguration> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<FlightApplicationConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(FlightApplicationConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)));
    }
}