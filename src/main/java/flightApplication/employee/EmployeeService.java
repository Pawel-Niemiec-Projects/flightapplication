package flightApplication.employee;

import org.skife.jdbi.v2.DBI;

public class EmployeeService {
    private EmployeeDao employeeDao;
    private DBI jdbi;

    public EmployeeService(EmployeeDao employeeDao, DBI jdbi) {
        this.employeeDao = employeeDao;
        this.jdbi = jdbi;
    }
}
